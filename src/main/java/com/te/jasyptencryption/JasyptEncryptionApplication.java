package com.te.jasyptencryption;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

@SpringBootApplication
@EnableJpaRepositories("com.te.jasyptencryption.repository")
@EnableEncryptableProperties
public class JasyptEncryptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(JasyptEncryptionApplication.class, args);
	}

}
