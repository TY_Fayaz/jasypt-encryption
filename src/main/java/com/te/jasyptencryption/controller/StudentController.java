package com.te.jasyptencryption.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.jasyptencryption.entity.Student;
import com.te.jasyptencryption.services.StudentService;

@RestController
public class StudentController {

	@Autowired
	StudentService studentService;

	@GetMapping("/getAll-student")
	public List<Student> getAllStudent() {
		return studentService.getAllStudent();
	}
}
