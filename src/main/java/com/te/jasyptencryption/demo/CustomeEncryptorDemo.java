package com.te.jasyptencryption.demo;

import org.jasypt.encryption.StringEncryptor;

import com.te.jasyptencryption.configuration.JasyptConfiguration;

public class CustomeEncryptorDemo {

	public static void main(String[] args) {
		JasyptConfiguration configuration = new JasyptConfiguration();
		StringEncryptor stringEncryptor = configuration.encryptor();
		String username="root";
		String password="root";
		String encryptUsername = stringEncryptor.encrypt(username);
		String encryptPassword = stringEncryptor.encrypt(password);
		System.err.println("encrypted username:" +encryptUsername);
		System.err.println("encrypted password:" +encryptPassword);

	}
}
